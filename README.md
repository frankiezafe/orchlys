# orchlys

[website](http://orchlys.frankiezafe.org/)

Orchlys is an experimental interspecies video game that interweaves plant observations with the issues of human intersex.
The proposal does not aim to be a tool to educate public but a plastic, poetic and sensitive work, whose ambition is to fantasize on the foreign beauty of this intermingling of living things.
This play is also a pretext to highlight intersex activism, represented in the acronym LGBTQI+ and still largely ignored in the media and institutional communication. Most of these people undergo multiple mutilations and/or heavy hormonal treatments in order to fit them into the "phallocrat" categories imposed on all human bodies.
Not constrained by this binary vision, the plant world demonstrates the richness of variations in gender, coexistence and individuality.
Orchlys is based on Pascale's 3d ecofeminist modeling researches and François' algorithmic works.

by [Pascale Barret](http://pascalebarret.com) & [François Zajéga](frankiezafe.org).

Under CC0 and MIT license.